FROM php:7.3-alpine

# Install PHP extensions
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin
RUN install-php-extensions xdebug gd zip

RUN echo "xdebug.mode=coverage" >> "$PHP_INI_DIR/php.ini"

# Install composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# Install GIT
RUN apk add --no-cache \
    git
